import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

declare var $: any;

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  book:any = {
    name:'',
    type:'',
    amount:''
  };
  bookEdit:any = {
    name:'',
    type:'',
    amount:''
  };

  filterName = ''

  tempBookIndex = null;

  bookList = [
    {
      name:'สูตรอาหาร1',
      type:'นิตยสาร',
      amount:'1'
    },
    {
      name:'สูตรอาหาร2',
      type:'นิตยสาร',
      amount:'1'
    },
  ];

  bookListTemp = []
  isFilter = false;


  bookname=''

  constructor(private modalService: NgbModal) { }

  closeResult = '';
  ngOnInit(): void {
  }

  addBook(){
    this.bookList.push(this.book);
    this.book = {
      name:'',
      type:'',
      amount:''
      
    };
    $('#exampleModal').modal('hide'); 
    console.log(this.bookList)
  }

  editBook(i){
    this.tempBookIndex = i;
    this.bookEdit = this.bookList[i];
  }

  saveBook(){
    this.bookList[this.tempBookIndex] = this.bookEdit;
    this.tempBookIndex = null;
    this. bookEdit = {
      name:'',
      type:'',
      amount:''
    };
    
    $('#exampleModalEdit').modal('hide'); 
  }

  delBook(i){
    // console.log(i)
    const temp = this.bookList.filter((item,index) => {
      // if(index == i)
      //   return false
      return index != i
    })
    this.bookList = temp;
  }
  
  search(){
    console.log(this.filterName);
    if(this.filterName == ''){
      if(this.isFilter == true){
        this.bookList = this.bookListTemp;
        this.isFilter = false;
      }
    } else {
      if(this.isFilter == false) {
        this.bookListTemp = this.bookList;
      }
      this.isFilter = true;

      this.bookList = this.bookListTemp.filter(item => {
        return item.name.indexOf(this.filterName) !== -1
    })  
    }
  }
}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from './module/test/test.component';
import { TesttestComponent } from './module/testtest/testtest.component';
import { BookComponent } from './module/book/book.component';


const routes: Routes = [
  {path:'index',component:BookComponent},
  {path:'',redirectTo:'/index',pathMatch:'full'},
  {path:'test',component:TestComponent},
  {path:'test2',component:TesttestComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
